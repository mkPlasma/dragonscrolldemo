#include "Player.h"
#include "ItemList.h"

Player::Player(int classType){
	classType_ = classType;
	init();
}

// Initialize player to class default stats/equips
void Player::init(){

	switch(classType_){

	case KNIGHT:
		hp_ = maxHp_ = bMaxHp_ = 200;
		mp_ = maxMp_ = bMaxMp_ = 100;
		mainWeapon_ = &itemWeapons[0];
		specialWeapon_ = &itemMagicWeapons[0];
		armor_ = &itemArmors[0];
		break;

	case ARCHER:
		hp_ = maxHp_ = bMaxHp_ = 150;
		mp_ = maxMp_ = bMaxMp_ = 150;
		break;

	case MAGE:
		hp_ = maxHp_ = bMaxHp_ = 100;
		mp_ = maxMp_ = bMaxMp_ = 200;
		break;
	}

	invSize_ = 20;
	updateStats();
}

void Player::updateStats(){
	def_ = armor_->def_;

	Entity::updateStats();
}

int Player::getClassType(){
	return classType_;
}

int Player::getXp(){
	return xp_;
}

int Player::getXpReq(){
	return xpReq_;
}

int Player::getLevel(){
	return level_;
}

ItemWeapon* Player::getMainWeapon(){
	return mainWeapon_;
}

ItemWeapon* Player::getSpecialWeapon(){
	return specialWeapon_;
}

ItemEquippable* Player::getArmor(){
	return armor_;
}

vector<InvItem>* Player::getInventory(){
	return &inventory_;
}

int Player::getInvSize(){
	return invSize_;
}