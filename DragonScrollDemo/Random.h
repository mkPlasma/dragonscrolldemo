#pragma once

#include<random>

using std::mt19937_64;
using std::uniform_real_distribution;
using std::uniform_int_distribution;


class Random{

	// Random number generator
	mt19937_64 rand;
	uniform_real_distribution<float> dist;

public:

	Random();

	float getRandFloat();
	bool getRandSuccess(float chance);

	int getRandInt(int min, int max);
};