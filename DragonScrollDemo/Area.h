#pragma once

#include<iostream>
#include<vector>

#include "Enemy.h"

using std::string;
using std::vector;


struct AreaEnm{
	Enemy* enemy_;
	float chance_;
};

struct Area{
	string name_;
	float enemySpawnChance;
	vector<AreaEnm> enemies_;
};