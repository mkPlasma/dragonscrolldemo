#pragma once

#include<vector>

#include "Attack.h"


using std::string;
using std::vector;


struct Item{
	string name_;
	int stackLimit_;

	Item(string name, int stackLimit){
		name_ = name;
		stackLimit_ = stackLimit;
	}
};

struct ItemConsumable : Item{
	int hp_;
	int mp_;
	vector<StatusEffect>* statusEffects_;
	
	ItemConsumable(string name, int stackLimit, int hp, int mp, vector<StatusEffect>* statusEffects) : Item(name, stackLimit){
		hp_ = hp;
		mp_ = mp;
		statusEffects_ = statusEffects;
	};

	ItemConsumable(string name, int stackLimit, int hp, int mp) : ItemConsumable(name, stackLimit, hp, mp, nullptr){}
};

struct ItemEquippable : Item{
	int classType_;

	int hp_;
	int mp_;
	int def_;

	int statusImmunities_;

	ItemEquippable(string name, int classType, int hp, int mp, int def, int statusImmunities) : Item(name, 1){
		classType_ = classType;
		hp_ = hp;
		mp_ = mp;
		def_ = def;
		statusImmunities_ = statusImmunities;
	};

	ItemEquippable(string name, int classType, int def) : ItemEquippable(name, classType, 0, 0, def, 0){};
};

struct ItemWeapon : ItemEquippable{
	bool isMagic_;
	vector<Attack> attacks_;

	ItemWeapon(string name, int classType, bool isMagic, vector<Attack> attacks) : ItemEquippable(name, classType, 0){
		isMagic_ = isMagic;
		attacks_ = attacks;
	};
};
