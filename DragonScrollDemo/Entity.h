#pragma once

class Entity{

protected:
	int hp_, mp_;
	int maxHp_, maxMp_;
	int bMaxHp_, bMaxMp_;
	int def_;

public:

	Entity(){};
	Entity(int hp, int mp, int def);

	void updateStats();

	void setHp(int hp);
	int getHp();
	void setMp(int mp);
	int getMp();

	int getMaxHp();
	int getMaxMp();
	int getDef();
	
};