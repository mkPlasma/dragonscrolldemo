#include "CommandHandler.h"
#include "ItemList.h"


CommandHandler::CommandHandler(GameData* data){
	data_ = data;
	player_ = &data_->player_;
	area_ = &data->area_;

	rand = Random();
}

CommandHandler::~CommandHandler(){
	delete data_;
}

// Main game loop
void CommandHandler::mainLoop(){

	while(1){
		cout << "\nEnter a command:" << endl;
		command();
	}
}

// Main command menu
void CommandHandler::command(){

	string cmd;
	int index = InputUtil::prompt(commands, true, &cmd);
	cout << endl;
	
	string arg;

	// Get args and isolate command
	if(cmd.find(" ") != string::npos){
		arg = cmd.substr(cmd.find_first_of(' ') + 1);
		cmd = cmd.substr(0, cmd.find_first_of(' '));
	}

	switch(index){

		// help
	case 0:
		break;


		// inventory
	case 1: case 2: case 3:
		inventory();
		break;


		//equip
	case 15: case 16:
		equip(arg);
		break;


		//drop
	case 17: case 18:
		break;


		// stats
	case 19: case 20:
		stats();
		break;


		// health
	case 21: case 22:
		cout << "Your health is " << player_->getHp() << "/" << player_->getMaxHp() << "." << endl;
		break;


		// magic
	case 23: case 24:
		cout << "Your magic is " << player_->getMp() << "/" << player_->getMaxMp() << "." << endl;
		break;

		
		// defense
	case 25: case 26:
		cout << "Your defense is " << player_->getDef() << "." << endl;
		break;

		//
	case 42: case 43:
		randBattle();
		break;

		// give
	case 50:
		give(arg);
		break;
	}
}

void CommandHandler::stats(){
	cout << "Your health is " << player_->getHp() << "/" << player_->getMaxHp() << "." << endl;
	cout << "Your magic is " << player_->getMp() << "/" << player_->getMaxMp() << "." << endl;
	cout << "Your defense is " << player_->getDef() << "." << endl;
}

void CommandHandler::inventory(){
	
	vector<InvItem>* inv = player_->getInventory();

	// Exit if inventory is empty
	if(inv->size() == 0){
		cout << "Your inventory is empty." << endl;
		return;
	}

	cout << "Inventory (" << inv->size() << "/" << player_->getInvSize() << ")\nType an item name for more info." << endl;

	for(InvItem i : *inv){
		cout << i.item_->name_ << (i.count_ != 0 ? "[" + std::to_string(i.count_) + "]" : "") << endl;
	}
}

void CommandHandler::equip(string arg){

	cout << "Your current equipment is:\n"
		 << player_->getMainWeapon()->name_ << "\n"
		 << player_->getSpecialWeapon()->name_ << "\n"
		 << player_->getArmor()->name_ << endl;
}

void CommandHandler::give(string arg){
	
	bool hasArg = arg != "";

	if(!hasArg){
		cout << "Enter an item to give:" << endl;

		// Get pointers to all items
		vector<Item*> items;

		for(Item i : itemConsumables)	items.push_back(&i);
		for(Item i : itemArmors)		items.push_back(&i);
		for(Item i : itemWeapons)		items.push_back(&i);
		for(Item i : itemMagicWeapons)	items.push_back(&i);

		// Get all item names
		vector<string> itemNames;

		for(Item* i : items)
			itemNames.push_back(i->name_);
		
		// Get item index
		int index = InputUtil::prompt(itemNames);

		player_->getInventory()->push_back(InvItem{items[index], 1});
	}
}


// Initiate battle with random enemy in area
void CommandHandler::randBattle(){

	// Reference random value
	float n = rand.getRandFloat();

	// Current chance sum to compare with
	float sum = 0;

	for(AreaEnm e : area_->enemies_){
		sum += e.chance_;

		// Spawn enemy if sum exceeds random value
		if(n < sum){
			battle(*e.enemy_);
			break;
		}
	}
}

// Battle event
void CommandHandler::battle(Enemy enemy){

	cout << "You have encountered a " + enemy.getName() + "!" << endl;

	bool enemyTurn = false;

	while(1){

		// Attack, its user and its target
		Entity* user, *target;
		string userName, targetName;
		Attack* attack = nullptr;
		string atkName = "";

		if(enemyTurn){
			user = &enemy;
			target = player_;
			userName = "The enemy";
			targetName = "You";
		}
		else{
			user = player_;
			target = &enemy;
			userName = "You";
			targetName = "The enemy";
		}

		// Get random enemy attack
		if(enemyTurn){

			float n = rand.getRandFloat();
			float sum = 0;

			for(EnmAttack a : enemy.getMoveset()){
				sum += a.chance_;

				if(n < sum){
					attack = &a.attack_;
					atkName = attack->name_;
					break;
				}
			}
		}

		// User input menu
		else{
			// Certain inputs won't use turn
			bool turnUsed = false;

			menu:
			while(!turnUsed){

				// Input options
				vector<string> accepted = {"Attack", "Inventory", "Stats", "Examine enemy", "Run"};

				cout << "\nWhat will you do?" << endl;

				for(string s : accepted)
					cout << s << endl;

				int index = InputUtil::prompt(accepted);
				cout << endl;

				switch(index){

					// attack
				case 0:{
					cout << "Normal attacks:" << endl;

					vector<Attack>* mainAttacks = &player_->getMainWeapon()->attacks_;
					vector<Attack>* specialAttacks = &player_->getSpecialWeapon()->attacks_;

					// Add all attacks
					vector<string> accepted;
					accepted.push_back("back");

					for(Attack a : *mainAttacks){
						cout << a.name_ << endl;
						accepted.push_back(a.name_);
					}

					int mp = player_->getMp();
					cout << "\nSpecial attacks (" << mp << "/" << player_->getMaxMp() << " MP):" << endl;

					for(Attack a : *specialAttacks){
						cout << a.name_ << endl;
						accepted.push_back(a.name_);
					}

					while(1){
						// Get attack
						int index = InputUtil::prompt(accepted);

						// If back
						if(index == 0)
							goto menu;

						index--;

						attack = index >= mainAttacks->size() ? &specialAttacks->operator[](index) : &mainAttacks->operator[](index);

						// Check magic cost
						if(attack->mpCost_ > mp)
							cout << "You don't have enough magic!" << endl;
						else{
							// Subtract magic
							target->setMp(target->getMp() - attack->mpCost_);

							turnUsed = true;
							break;
						}
					}

					break;
				}

				case 1:
					inventory();
					break;

				case 2:
					stats();
					break;

					// examine
				case 3:
					cout << "the enemy's dark magic magic makes it unexaminable" << endl;
					break;

					// run
				case 4:
					cout << "the developer has not yet accounted for your incompetence, you coward" << endl;
					break;
				}
			}
		}

		// Execute attack
		
		// Check that attack exists
		if(attack == nullptr){
			enemyTurn = !enemyTurn;
			continue;
		}

		cout << "\n" << userName << " used " << (enemyTurn ? atkName : attack->name_) << "!" << endl;

		// Deal damage
		int dmg = rand.getRandInt(attack->minDamage_, attack->maxDamage_);
		target->setHp(target->getHp() - dmg);

		cout << "\nIt dealt " << dmg << " damage!" << endl;
		
		if(enemyTurn)
			cout << targetName << " now have " << target->getHp() << "/" << target->getMaxHp() << " health left." << endl;
		else
			cout << targetName << " now has " << target->getHp() << "/" << target->getMaxHp() << " health left." << endl;

		enemyTurn = !enemyTurn;
	}
}