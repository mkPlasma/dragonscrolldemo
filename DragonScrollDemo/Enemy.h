#pragma once

#include<iostream>
#include <vector>

#include "Entity.h"
#include "Item.h"
#include "Attack.h"


using std::string;
using std::vector;


struct EnmAttack{
	Attack attack_;
	float chance_;
};

struct EnmDrop{
	Item* items_;
	float chance_;
};

class Enemy : public Entity{
	string name_;

	int xpMin_, xpMax_;
	int goldMin_, goldMax_;

	vector<EnmDrop> drops_;
	vector<EnmAttack> moveset_;

public:

	Enemy(string name, int hp, int mp, int def, int xpMin, int xpMax, int goldMin, int goldMax, vector<EnmAttack> moveset, vector<EnmDrop> drops);

	string getName();

	int getXpMin();
	int getXpMax();
	int getGoldMin();
	int getGoldMax();

	vector<EnmAttack> getMoveset();
	vector<EnmDrop> getDrops();
};