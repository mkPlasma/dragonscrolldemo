#pragma once

#include<iostream>


struct StatusEffect{

	static enum types{
		HP_REGEN,
		MP_REGEN,
		HP_BUFF,
		MP_BUFF,
		DEF_BUFF,
		DMG_BUFF,
		DMG_BLOCK,
		DMG_TO_HP,

		HP_DRAIN_BLEED,
		HP_DRAIN_BURN,
		MP_DRAIN_POISON,
		MP_DRAIN_STEAL,
		HP_DEBUFF,
		MP_DEBUFF,
		DEF_DEBUFF,
		DMG_DEBUFF,
		DMG_FAIL
	};

	int type_;
	int level_;
	int duration_;

	static std::string getName(int type){
		switch(type){
		case HP_REGEN:			return "Health regen";
		case MP_REGEN:			return "Magic regen";
		case HP_BUFF:			return "Health increase";
		case MP_BUFF:			return "Magic increase";
		case DEF_BUFF:			return "Defense increase";
		case DMG_BUFF:			return "Strength";
		case DMG_BLOCK:			return "Barrier";
		case DMG_TO_HP:			return "Absorption";

		case HP_DRAIN_BLEED:	return "Bleed";
		case HP_DRAIN_BURN:		return "Burn";
		case MP_DRAIN_POISON:	return "Poison";
		case MP_DRAIN_STEAL:	return "Magic steal";
		case HP_DEBUFF:			return "Health decrease";
		case MP_DEBUFF:			return "Magic decrease";
		case DEF_DEBUFF:		return "Defense break";
		case DMG_DEBUFF:		return "Weakness";
		case DMG_FAIL:			return "Confusion";
		}
	}
};