#pragma once

#include "Item.h"
#include "Player.h"

static vector<ItemEquippable> itemArmors = *new vector<ItemEquippable>{
	
	//Knight
	
	//0
	ItemEquippable("Iron Armor", Player::KNIGHT, 5),

	//1
	ItemEquippable("Chainmail Armor",Player::KNIGHT,6),

	//2
	ItemEquippable("Chainmail Armor",Player::KNIGHT,9),

	//3
	ItemEquippable("Silver Armor",Player::KNIGHT,0),

	//4
	ItemEquippable("Golden Armor",Player::KNIGHT,0),

	//5
	ItemEquippable("Shadow Steel Armor",Player::KNIGHT,0),

	//6
	ItemEquippable("Knight Armor",Player::KNIGHT,0),

	//7
	ItemEquippable("Bluesteel Armor",Player::KNIGHT,0),

	//8
	ItemEquippable("Frost Armor",Player::KNIGHT,0),

	//9
	ItemEquippable("Volcanic Armor",Player::KNIGHT,0),

	//10
	ItemEquippable("Ironrock Armor",Player::KNIGHT,0),

	//11
	ItemEquippable("Magic Iron Armor",Player::KNIGHT,0),

	//12
	ItemEquippable("Dragonstone Armor",Player::KNIGHT,0),

	//13
	ItemEquippable("Inorexite Armor",Player::KNIGHT,0),

	//14
	ItemEquippable("Cosmic Armor",Player::KNIGHT,0),

	//Archer

	//15
	ItemEquippable("Leather Shirt",Player::ARCHER,3),

	//16
	ItemEquippable("Leather Armor",Player::ARCHER,5),

	//17
	ItemEquippable("Hardened Leather Armor",Player::ARCHER,7),

	//18
	ItemEquippable("Reinforced Leather Armor",Player::ARCHER,10),

	//19
	ItemEquippable("Bear Hide Armor",Player::ARCHER,0),

	//20
	ItemEquippable("Lizard Skin Armor",Player::ARCHER,0),

	//21
	ItemEquippable("Komodo Hide Armor",Player::ARCHER,0),

	//22
	ItemEquippable("Wolf Armor",Player::ARCHER,0),

	//23
	ItemEquippable("Serpent Armor",Player::ARCHER,0),

	//24
	ItemEquippable("Griffon Armor",Player::ARCHER,0),

	//25
	ItemEquippable("Dragon Armor",Player::ARCHER,0),

	//26
	ItemEquippable("Hydra Armor",Player::ARCHER,0),

	//27
	ItemEquippable("Inorexite Leather Armor",Player::ARCHER,0),

	//28
	ItemEquippable("Dark Armor",Player::ARCHER,0),

	//29
	ItemEquippable("Glaxian Armor",Player::ARCHER,0),

	//Mage

	//30
	ItemEquippable("Leather Robe",Player::MAGE,1),

	//31
	ItemEquippable("Silk Robe",Player::MAGE,2),

	//32
	ItemEquippable("Apprentice's Robe",Player::MAGE,4),

	//33
	ItemEquippable("Mage Robe",Player::MAGE,0),

	//34
	ItemEquippable("Wizard Robe",Player::MAGE,0),

	//35
	ItemEquippable("Sorcerer Robe",Player::MAGE,0),

	//36
	ItemEquippable("Metallic Robe",Player::MAGE,0),

	//37
	ItemEquippable("Warlock Robe",Player::MAGE,0),

	//38
	ItemEquippable("Necromancer Robe",Player::MAGE,0),

	//39
	ItemEquippable("Earth Robe",Player::MAGE,0),

	//40
	ItemEquippable("Flame Robe",Player::MAGE,0),

	//41
	ItemEquippable("Isolation Robe",Player::MAGE,0),

	//42
	ItemEquippable("Starsilk",Player::MAGE,0),

	//43
	ItemEquippable("Stardust Robe",Player::MAGE,0),

	//44
	ItemEquippable("Nebula Robe",Player::MAGE,0),
	
};