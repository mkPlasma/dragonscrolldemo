#pragma once
#include "Player.h"
#include "ItemList.h"
#include "Enemy.h"


static vector<Enemy> enemies = *new vector<Enemy>{

	//name,hp,mp,def,xpmin,xpmax,goldmin,goldmax
	Enemy("Lesser Goblin", 75, 150, 3, 10, 25, 5, 15,
		vector<EnmAttack>{
			{Attack("Thrash", 1, 15, 1, 0.3, 0, nullptr), 1}
		},
		vector<EnmDrop>{
			{&itemWeapons[28],0.05f},
			{&itemConsumables[0],0.95f}
		}
	),

	Enemy("Goblin", 110, 175, 5, 25, 33, 15, 25,
		vector<EnmAttack>{
			{Attack("Flail", 1, 10, .4f, 0.02f, 0, nullptr), 1},
			{Attack("Pummel", 8, 25, .5f, 0.02f, 0, nullptr), 1},
			{Attack("Slash", 12, 25, .1f, 0.25f, 0, nullptr), 1}
		},
		vector<EnmDrop>{
			{&itemWeapons[28],0.05f},
			{&itemConsumables[0],0.95f}
		}
	),

	Enemy("Greater Goblin", 200, 250, 9, 45, 60, 50, 85,
		vector<EnmAttack>{
			{Attack("Flail", 1, 10, .13f, 0.02f, 0, nullptr), 1},
			{Attack("Pummel", 8, 25, .3f, 0.02f, 0, nullptr), 1},
			{Attack("Slash", 12, 25, .32f, 0.15f, 0, nullptr), 1},
			{Attack("Crush", 10, 15, .25f, 0, 0, &vector<AttackStatus>{
				{StatusEffect{StatusEffect::DEF_DEBUFF, 10, 2}, .5f}
			}), 1}
		},
		vector<EnmDrop>{
			{&itemWeapons[28],0.05f},
			{&itemWeapons[0],0.05f},
			{&itemWeapons[1],0.05f},
			{&itemWeapons[2],0.05f},
			{&itemWeapons[13],0.05f},
			{&itemWeapons[15],0.05f},
			{&itemWeapons[17],0.05f},
			{&itemWeapons[22],0.05f},
			{&itemWeapons[23],0.05f},
			{&itemWeapons[24],0.05f},
			{&itemConsumables[0],0.1f}
		}
	),
	
	Enemy("Arachtoid", 120, 250, 0, 20, 30, 40, 50,
		vector<EnmAttack>{
			{Attack("Bite", 5, 18, .5f, 0.02f, 0, nullptr), 1},
			{Attack("Tether", 0, 5, .5f, 0.02f, 0, &vector<AttackStatus>{
				{StatusEffect{StatusEffect::MP_DEBUFF, 50, 1}, .5f},
				{StatusEffect{StatusEffect::DMG_FAIL, 5, 1}, .5f}}), 1}
			},
		vector<EnmDrop>{
			{&itemWeapons[19
			],0.05f}
		}
	)
};
