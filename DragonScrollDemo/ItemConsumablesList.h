#pragma once

#include "Item.h"

static vector<ItemConsumable> itemConsumables = *new vector<ItemConsumable>{
	//0
	//name,carrylimit,hp,mp
	ItemConsumable("Rag",10,15,0,nullptr),
	//1
	ItemConsumable("Basic Health Potion",10,30,0,nullptr),
	//2
	ItemConsumable("Health Potion",10,60,0,nullptr),
	//3
	ItemConsumable("Strong Health Potion",10,120,0,nullptr),
	//4
	ItemConsumable("Basic Magic Potion",10,0,30,nullptr),
	//5
	ItemConsumable("Magic Potion",10,0,60,nullptr),
	//6
	ItemConsumable("Strong Magic Potion",10,0,120,nullptr),
	//7
	ItemConsumable("Basic Regen Potion",10,0,0,&vector<StatusEffect>{
				{StatusEffect{StatusEffect::HP_REGEN, 10, 2}
		}}),
	//8
	ItemConsumable("Regen Potion",10,0,0,&vector<StatusEffect>{
				{StatusEffect{StatusEffect::HP_REGEN, 30, 3}
		}}),
	//9
	ItemConsumable("Strong Regen Potion",10,0,0,&vector<StatusEffect>{
				{StatusEffect{StatusEffect::HP_REGEN, 50, 4}
		}}),
	//10
	ItemConsumable("Basic Magic Regen Potion",10,0,0,&vector<StatusEffect>{
				{StatusEffect{StatusEffect::MP_REGEN, 10, 2}
		}}),
	//11
	ItemConsumable("Basic Magic Regen Potion",10,0,0,&vector<StatusEffect>{
				{StatusEffect{StatusEffect::MP_REGEN, 30, 3}
		}}),
	//12
	ItemConsumable("Strong Magic Regen Potion",10,0,0,&vector<StatusEffect>{
				{StatusEffect{StatusEffect::MP_REGEN, 50, 4}
		}}),
	//13
	ItemConsumable("Basic Restoration Potion",5,30,30,nullptr),
	//14
	ItemConsumable("Restoration Potion",5,60,60,nullptr),
	//15
	ItemConsumable("Strong Restoration Potion",5,120,120,nullptr),
	//16
	ItemConsumable("Life Potion",10,250,0,nullptr),
	//17
	ItemConsumable("Mana Potion",10,0,250,nullptr),
	//18
	ItemConsumable("Spider Web",0,0,0,&vector<StatusEffect>{
				{StatusEffect{StatusEffect::DMG_FAIL, 30, 1}
		}}),
	//19
	ItemConsumable("Bottle of Spider Venom", 0, 0, 0, &vector<StatusEffect>{
				{StatusEffect{StatusEffect::MP_DRAIN_POISON, 30, 2}
		}})

};