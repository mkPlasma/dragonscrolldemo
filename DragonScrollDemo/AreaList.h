#pragma once

#include "Area.h"
#include "EnemyList.h"

static vector<Area> areas = *new vector<Area>{
	//0
	Area{"Silvian Town",0,
		vector<AreaEnm>{{}
		}
	},
	//1
	Area{"Dithered Forest",0.2f,
		vector<AreaEnm>{
			{&enemies[0],1}
		}
	}
};