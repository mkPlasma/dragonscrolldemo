#pragma once

#include "Item.h"

static vector<ItemWeapon> itemWeapons = *new vector<ItemWeapon>{
	//0
	ItemWeapon("Iron Sword", Player::KNIGHT, false, vector<Attack>{
		Attack{"Basic Slash", 15, 20, 0.85f, 0.02f, 0, nullptr},
		Attack{"Strong Slash", 25, 35, 0.60f, 0.02f, 0, nullptr},
	}),
	//1
	ItemWeapon("Lucky Iron Sword", Player::KNIGHT, false, vector<Attack>{
		Attack{"Basic Slash", 15, 20, 0.85f, 0.05f, 0, nullptr},
		Attack{"Strong Slash", 25, 35, 0.60f, 0.05f, 0, nullptr},
	}),
	//2
	ItemWeapon("Well Made Iron Sword", Player::KNIGHT, false, vector<Attack>{
		Attack{"Basic Slash", 17, 20, 0.85f, 0.02f, 0, nullptr},
		Attack{"Strong Slash", 30, 35, 0.60f, 0.02f, 0, nullptr},
	}),
	//3
	ItemWeapon("Well Balanced Iron Sword", Player::KNIGHT, false, vector<Attack>{
		Attack{"Basic Slash", 15, 20, 0.90f, 0.02f, 0, nullptr},
		Attack{"Strong Slash", 25, 35, 0.65f, 0.02f, 0, nullptr},
	}),
	//4
	ItemWeapon("Powerful Iron Sword", Player::KNIGHT, false, vector<Attack>{
		Attack{"Basic Slash", 15, 25, 0.85f, 0.02f, 0, nullptr},
		Attack{"Strong Slash", 25, 40, 0.60f, 0.02f, 0, nullptr},
	}),
	//5
	ItemWeapon("Steel Sword", Player::KNIGHT, false, vector<Attack>{
		Attack{"Basic Slash", 20, 25, 0.85f, 0.02f, 0, nullptr},
		Attack{"Strong Slash", 30, 40, 0.60f, 0.02f, 0, nullptr},
	}),
	//6
	ItemWeapon("Broken Sword?", Player::KNIGHT, false, vector<Attack>{
		Attack{"Weak Slash", 5, 10, 0.90f, 0.0001f, 0, nullptr},
		Attack{"Basic Slash", 15, 20, 0.85f, 0.0001f, 0, nullptr},
	}),
	//7
	ItemWeapon("Artisan Sword", Player::KNIGHT, false, vector<Attack>{
		Attack{"Basic Slash", 25, 30, 0.85f, 0.02f, 0, nullptr},
		Attack{"Strong Slash", 35, 45, 0.60f, 0.02f, 0, nullptr},
	}),
	//8
	ItemWeapon("Cutlass", Player::KNIGHT, false, vector<Attack>{
		Attack{"Basic Slash", 15, 25, 0.80f, 0.04f, 0, nullptr},
		Attack{"Quick Slash", 25, 40, 0.60f, 0.10f, 0, nullptr},
	}),
	//9
	ItemWeapon("Iron Hammer", Player::KNIGHT, false, vector<Attack>{
		Attack{"Basic Smash", 15, 30, 0.70f, 0.001f, 0, nullptr},
		Attack{"Grand Slam", 35, 45, 0.50f, 0.001f, 0, nullptr},
	}),
	//10
	ItemWeapon("Steel Hammer", Player::KNIGHT, false, vector<Attack>{
		Attack{"Basic Smash", 20, 40, 0.70f, 0.001f, 0, nullptr},
		Attack{"Grand Slam", 40, 50, 0.50f, 0.001f, 0, nullptr},
	}),
	//11
	ItemWeapon("Artisan Hammer", Player::KNIGHT, false, vector<Attack>{
		Attack{"Basic Smash", 25, 45, 0.70f, 0.001f, 0, nullptr},
		Attack{"Grand Slam", 45, 55, 0.50f, 0.001f, 0, nullptr},
	}),
	//12
	ItemWeapon("Wooden Bow", Player::ARCHER, false, vector<Attack>{
		Attack{"Basic Shot", 15, 20, 0.85f, 0.02f, 0, nullptr},
		Attack{"Long Shot", 25, 35, 0.60f, 0.02f, 0, nullptr},
	}),
	//13
	ItemWeapon("Iron Bow", Player::ARCHER, false, vector<Attack>{
		Attack{"Basic Shot", 20, 25, 0.85f, 0.02f, 0, nullptr},
		Attack{"Long Shot", 30, 40, 0.60f, 0.02f, 0, nullptr},
	}),
	//14
	ItemWeapon("Silver Bow", Player::ARCHER, false, vector<Attack>{
		Attack{"Basic Shot", 25, 30, 0.85f, 0.02f, 0, nullptr},
		Attack{"Long Shot", 35, 45, 0.60f, 0.02f, 0, nullptr},
	}),
	//15
	ItemWeapon("Driftwood Bow", Player::ARCHER, false, vector<Attack>{
		Attack{"Weak Shot", 5, 10, 0.90f, 0.001f, 0, nullptr},
		Attack{"Basic Shot", 15, 20, 0.85f, 0.001f, 0, nullptr},
	}),
	//16
	ItemWeapon("Artisan Bow", Player::ARCHER, false, vector<Attack>{
		Attack{"Basic Shot", 30, 35, 0.85f, 0.02f, 0, nullptr},
		Attack{"Long Shot", 40, 50, 0.60f, 0.02f, 0, nullptr},
	}),
	//17
	ItemWeapon("long Bow", Player::ARCHER, false, vector<Attack>{
		Attack{"Basic Shot", 10, 15, 0.75f, 0.02f, 0, nullptr},
		Attack{"Long Shot", 25, 40, 0.70f, 0.04f, 0, nullptr},
	}),
	//18
	ItemWeapon("Iron long Bow", Player::ARCHER, false, vector<Attack>{
		Attack{"Basic Shot", 15, 20, 0.75f, 0.02f, 0, nullptr},
		Attack{"Long Shot", 30, 45, 0.70f, 0.04f, 0, nullptr},
	}),
	//19
	ItemWeapon("Silver long Bow", Player::ARCHER, false, vector<Attack>{
		Attack{"Basic Shot", 20, 25, 0.75f, 0.02f, 0, nullptr},
		Attack{"Long Shot", 35, 50, 0.70f, 0.04f, 0, nullptr},
	}),
	//20
	ItemWeapon("Artisan long Bow", Player::ARCHER, false, vector<Attack>{
		Attack{"Basic Shot", 25, 30, 0.75f, 0.02f, 0, nullptr},
		Attack{"Long Shot", 40, 55, 0.70f, 0.04f, 0, nullptr},
	}),
	//21
	ItemWeapon("Wooden Staff", Player::MAGE, false, vector<Attack>{
		Attack{"Twack", 5, 10, 0.85f, 0.001f, 0, nullptr},
		Attack{"Wallop", 15, 20, 0.60f, 0.001f, 0, nullptr},
	}),
	//22
	ItemWeapon("Driftwooden Staff", Player::MAGE, false, vector<Attack>{
		Attack{"Twack", 1, 5, 0.85f, 0.001f, 0, nullptr},
		Attack{"Wallop", 10, 15, 0.60f, 0.001f, 0, nullptr},
	}),
	//23
	ItemWeapon("Steel Staff", Player::MAGE, false, vector<Attack>{
		Attack{"Twack", 10, 15, 0.85f, 0.001f, 0, nullptr},
		Attack{"Wallop", 20, 25, 0.60f, 0.001f, 0, nullptr},
	}),
	//24
	ItemWeapon("Artisan Staff", Player::MAGE, false, vector<Attack>{
		Attack{"Twack", 15, 20, 0.85f, 0.001f, 0, nullptr},
		Attack{"Wallop", 25, 30, 0.60f, 0.001f, 0, nullptr},
	}),
	//25
	ItemWeapon("Wooden Wand", Player::MAGE, false, vector<Attack>{
		Attack{"Twack", 1, 10, 0.90f, 0.02f, 0, nullptr},
		Attack{"Poke", 10, 20, 0.70f, 0.02f, 0, nullptr},
	}),
	//26
	ItemWeapon("Iron Wand", Player::MAGE, false, vector<Attack>{
		Attack{"Twack", 5, 15, 0.90f, 0.02f, 0, nullptr},
		Attack{"Poke", 15, 25, 0.70f, 0.02f, 0, nullptr},
	}),
	//27
	ItemWeapon("Artisan Wand", Player::MAGE, false, vector<Attack>{
		Attack{"Twack", 10, 20, 0.90f, 0.02f, 0, nullptr},
		Attack{"Poke", 20, 30, 0.70f, 0.02f, 0, nullptr},
	}),
	//28
	ItemWeapon("Small Dagger", Player::ARCHER, false, vector<Attack>{
		Attack{ "Jab", 10, 20, 0.70f, 0.08f, 0, nullptr },
		Attack{ "Pierce", 25, 40, .30f, 0.01f, 0, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::HP_DRAIN_BLEED, 10, 3}, 1}}
			},
		})
};