#include "Random.h"

#include<ctime>


Random::Random(){
	rand = mt19937_64();
	rand.seed(time(NULL));
	dist = uniform_real_distribution<float>(0, 1);
}

float Random::getRandFloat(){
	return dist(rand);
}

bool Random::getRandSuccess(float chance){
	return getRandFloat() > chance;
}

int Random::getRandInt(int min, int max){
	uniform_int_distribution<int> dist = uniform_int_distribution<int>(min, max);
	return dist(rand);
}