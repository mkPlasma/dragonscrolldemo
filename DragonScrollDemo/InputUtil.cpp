#include "InputUtil.h"

// Standard prompt without formatting
string InputUtil::promptRaw(){
	while(1){
		cout << "> ";
		cout.flush();

		string s;
		getline(cin, s);

		if(regex_match(s, regex("[\\s\\w]+")))
			return s;

		cout << "Invalid input, please try again!" << endl;
	}
}

// Standard prompt with formatting
string InputUtil::prompt(){
	string s = promptRaw();
	return TextUtil::format(s);
}

// Checks for exact or partial match of a string in an array
int InputUtil::getIndex(string input, vector<string> allowed, string rawInput, string* result){

	int index = -2;

	for(int i = 0; i < allowed.size(); i++){

		string s = TextUtil::toLower(allowed[i]);

		// Check exact match
		if(input == s){

			// Store matched string if given pointer
			if(result != nullptr)
				*result = rawInput;

			return i;
		}

		// Check partial match
		if(input == s.substr(0, input.length())){
			if(index == -2)
				index = i;
			// Fail if a second match is found
			else
				index = -1;
		}
	}

	// Return partial match
	if(index >= 0){

		// Store matched string if given pointer
		if(result != nullptr)
			*result = rawInput;

		return index;
	}

	return -1;
}

// Input prompt accepting only given strings
// Checks for exact match or shortest unique match
int InputUtil::prompt(vector<string> allowed, bool args, string* result){

	while(1){
		string rawInput = prompt();
		string input = rawInput;

		// Check only up to space if using args
		if(args)
			input = input.substr(0, input.find_first_of(' '));

		int index = getIndex(input, allowed, rawInput, result);

		if(index >= 0)
			return index;

		cout << "Invalid input, please try again!" << endl;
	}
}

// Yes/no prompt
bool InputUtil::promptYN(){
	cout << "(Y/N)" << endl;
	return prompt(vector<string>{"no", "yes"});
}