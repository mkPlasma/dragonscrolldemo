#pragma once

#include<regex>
#include<vector>

#include "TextUtil.h"

using std::cout;
using std::cin;
using std::endl;
using std::vector;


class InputUtil{
public:
	static string promptRaw();
	static string prompt();

	static int getIndex(string input, vector<string> allowed, string rawInput = "", string* result = nullptr);
	static int prompt(vector<string> allowed, bool args = false, string* result = nullptr);

	static bool promptYN();
};