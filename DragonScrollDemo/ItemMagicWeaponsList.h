#pragma once

#include "Item.h"

static vector<ItemWeapon> itemMagicWeapons = *new vector<ItemWeapon>{

	ItemWeapon("Wooden Shield", Player::KNIGHT, true, vector<Attack>{
		Attack{"Block", 0, 0, 1, 0, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DEF_BUFF, 4, 1}, 1}}
		},
		Attack{"Bash", 15, 20, 0.85f, 0.02f, 0, nullptr},
	}),

	ItemWeapon("Iron Shield", Player::KNIGHT, true, vector<Attack>{
		Attack{"Block", 0, 0, 1, 0, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DEF_BUFF, 5, 1}, 1}}
		},
		Attack{"Bash", 20, 25, 0.85f, 0.02f, 0, nullptr},
	}),

	ItemWeapon("Mirror Shield", Player::KNIGHT, true, vector<Attack>{
		Attack{"Block", 0, 0, 1, 0, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DEF_BUFF, 4, 1}, 1}}
		},
		Attack{"Magic Block", 0, 0, 1, 0, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DMG_BLOCK, 4, 1}, 0.8f}}
		},
		Attack{"Bash", 10, 15, 0.85f, 0.02f, 0, nullptr},
	}),

	ItemWeapon("Fire Sword", Player::KNIGHT, true, vector<Attack>{
		Attack{"Flame Strike", 15, 20, 1, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::HP_DRAIN_BURN, 4, 4}, 0.5f}}
		},
		Attack{"Basic Slash", 15, 20, 0.85f, 0.02f, 0, nullptr},
		Attack{"Strong Slash", 25, 35, 0.60f, 0.02f, 0, nullptr},
	}),

	ItemWeapon("Poison Sword", Player::KNIGHT, true, vector<Attack>{
		Attack{"Poison Strike", 15, 20, 1, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::MP_DRAIN_POISON, 4, 4}, 0.5f}}
		},
		Attack{"Basic Slash", 15, 20, 0.85f, 0.02f, 0, nullptr},
		Attack{"Strong Slash", 25, 35, 0.60f, 0.02f, 0, nullptr},
	}),

	ItemWeapon("Fire Quiver", Player::ARCHER, true, vector<Attack>{
		Attack{"Flame Arrow", 15, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::HP_DRAIN_BURN, 4, 4}, 0.70f}}
		},
		Attack{"Binding Arrow", 15, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DMG_DEBUFF, 4, 4}, 0.70f}}
		},
		Attack{"Piercing Arrow", 15, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DEF_DEBUFF, 4, 4}, 0.70f}}
		},
	}),

	ItemWeapon("Poison Quiver", Player::ARCHER, true, vector<Attack>{
		Attack{"Poison Arrow", 15, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::MP_DRAIN_POISON, 4, 4}, 0.70f}}
		},
		Attack{"Binding Arrow", 15, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DMG_DEBUFF, 4, 4}, 0.70f}}
		},
		Attack{"Piercing Arrow", 15, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DEF_DEBUFF, 4, 4}, 0.70f}}
		},
	}),

	ItemWeapon("Hunter's Quiver", Player::ARCHER, true, vector<Attack>{
		Attack{"Hunter's Arrow", 15, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::HP_DRAIN_BLEED, 4, 4}, 0.70f}}
		},
		Attack{"Binding Arrow", 15, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DMG_DEBUFF, 4, 4}, 0.70f}}
		},
		Attack{"Piercing Arrow", 15, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DEF_DEBUFF, 4, 4}, 0.70f}}
		},
	}),

	ItemWeapon("Basic Quiver", Player::ARCHER, true, vector<Attack>{
		Attack{"Binding Arrow", 15, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DMG_DEBUFF, 4, 4}, 0.70f}}
		},
		Attack{"Piercing Arrow", 15, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DEF_DEBUFF, 4, 4}, 0.70f}}
		},
	}),

	ItemWeapon("Wither Quiver", Player::ARCHER, true, vector<Attack>{
		Attack{"Wither Arrow", 15, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DMG_DEBUFF, 6, 4}, 0.70f}}
		},
		Attack{"Binding Arrow", 15, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DMG_DEBUFF, 4, 4}, 0.70f}}
		},
		Attack{"Piercing Arrow", 15, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DEF_DEBUFF, 4, 4}, 0.70f}}
		},
	}),

	ItemWeapon("Lowmack�s guild to cattrips and other almost useless magic", Player::MAGE, true, vector<Attack>{
		Attack{"Flame Strike", 5, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::HP_DRAIN_BURN, 4, 4}, 0.90f}}
		},
		Attack{"Poison Splash", 5, 20, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::MP_DRAIN_POISON, 4, 4}, 0.90f}}
		},
		Attack{"Healing Word", 0, 0, 1, 0, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::HP_REGEN, 4, 4}, 1}}
		},
	}),

	ItemWeapon("Midmack�s guild to intermediate and object-oriented magic", Player::MAGE, true, vector<Attack>{
		Attack{"Fire Ball", 20, 40, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::HP_DRAIN_BURN, 4, 4}, 0.90f}}
		},
		Attack{"Poison Wave", 20, 40, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::MP_DRAIN_POISON, 4, 4}, 0.90f}}
		},
		Attack{"Healing Touch", 0, 0, 1, 0, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::HP_REGEN, 6, 4}, 1}}
		},
	}),

	ItemWeapon("Darkmack�s black magic compendium", Player::MAGE, true, vector<Attack>{
		Attack{"Void Ball", 20, 40, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DEF_DEBUFF, 4, 4}, 0.90f}}
		},
		Attack{"Void Wave", 20, 40, 0.85f, 0.02f, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::HP_DEBUFF, 4, 4}, 0.90f}}
		},
		Attack{"Vamperic Touch", 20, 40, 1, 0, 20, &vector<AttackStatus>{
			{StatusEffect{StatusEffect::DMG_TO_HP, 6, 4}, 1}}
		},
	}),
};