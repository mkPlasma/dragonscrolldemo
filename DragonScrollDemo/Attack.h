#pragma once

#include<vector>

#include "StatusEffect.h";

using std::string;
using std::vector;

struct AttackStatus{
	StatusEffect effect_;
	float chance_;
};

struct Attack{
	string name_;

	int minDamage_, maxDamage_;
	float hitRate_, critRate_;

	int mpCost_;

	vector<AttackStatus>* statusEffects_;

	Attack(string name, int minDamage, int maxDamage, float hitRate, float critRate, int mpCost, vector<AttackStatus> *statusEffects) {
		name_ = name;
		minDamage_ = minDamage;
		maxDamage_ = maxDamage;
		hitRate_ = hitRate;
		critRate_ = critRate;
		mpCost_ = mpCost;
		statusEffects_ = statusEffects;
	}
};