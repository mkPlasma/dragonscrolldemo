#pragma once

#include<iostream>
#include<regex>


using std::string;
using std::regex;
using std::regex_match;
using std::regex_replace;


class TextUtil{
public:
	static string toLower(string& s);
	static string trim(string& s);
	static string format(string& s);
	static string capitalize(string& s);
};