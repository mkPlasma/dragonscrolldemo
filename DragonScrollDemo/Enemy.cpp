#include "Enemy.h"

Enemy::Enemy(string name, int hp, int mp, int def, int xpMin, int xpMax, int goldMin, int goldMax,
	vector<EnmAttack> moveset, vector<EnmDrop> drops) : Entity(hp, mp, def){
	name_ = name;
	xpMin_ = xpMin;
	xpMax_ = xpMax;
	goldMin_ = goldMin;
	goldMax_ = goldMax;
	drops_ = drops;
	moveset_ = moveset;
}


string Enemy::getName(){
	return name_;
}
int Enemy::getXpMin(){
	return xpMin_;
}

int Enemy::getXpMax(){
	return xpMax_;
}

int Enemy::getGoldMin(){
	return goldMin_;
}

int Enemy::getGoldMax(){
	return goldMax_;
}

vector<EnmAttack> Enemy::getMoveset(){
	return moveset_;
}

vector<EnmDrop> Enemy::getDrops(){
	return drops_;
}