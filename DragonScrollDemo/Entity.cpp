#include "Entity.h"

Entity::Entity(int hp, int mp, int def){
	hp_ = maxHp_ = bMaxHp_ = hp;
	mp_ = maxMp_ = bMaxMp_ = mp;
}

void Entity::updateStats(){
	
}

void Entity::setHp(int hp){
	hp_ = hp;
}

int Entity::getHp(){
	return hp_;
}

void Entity::setMp(int mp){
	mp_ = mp;
}

int Entity::getMp(){
	return mp_;
}

int Entity::getMaxHp(){
	return maxHp_;
}

int Entity::getMaxMp(){
	return maxMp_;
}

int Entity::getDef(){
	return def_;
}
