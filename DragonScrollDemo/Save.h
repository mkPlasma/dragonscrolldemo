#pragma once

#include "InputUtil.h"
#include "Player.h"
#include "Area.h"
#include "AreaList.h"


struct GameData{
	string saveName_;
	Player player_;
	Area area_;
};

class Save{
public:
	static void saveFile(GameData* data);
	static GameData* newSave();
	static GameData* loadSave();
};