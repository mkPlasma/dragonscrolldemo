#include "Save.h"

// Save this SaveInfo to a file
void Save::saveFile(GameData* data){

}

GameData* Save::newSave(){

	cout << "\nInput save name:" << endl;
	string name = InputUtil::prompt();

	cout << "\nPlease choose a player class:" << endl;

	int classType;

	while(1){
		for(string s : playerClassNames)
			cout << TextUtil::capitalize(s) << endl;

		classType = InputUtil::prompt(playerClassNames);

		cout << "\n" << TextUtil::capitalize(playerClassNames[classType]) << endl;

		// Display class info
		switch(classType){
		case Player::KNIGHT:
			cout << "HP: 200\nMP: 100\nDEF: 10\n" <<
				"The knight is a powerful, durable class that is powerful in early-game but weakens over time.\n" <<
				"He uses his sword to deal large amounts of damage, and shield to increase his defense." << endl;
			break;
		case Player::ARCHER:
			cout << "HP: 150\nMP: 150\nDEF: 7\n" <<
				"The archer is a mid-range class whose power stays consistent throughout the adventure.\n" <<
				"He uses his bow and arrows to damage and inconvenience enemies." << endl;
			break;
		case Player::MAGE:
			cout << "HP: 100\nMP: 200\nDEF: 5\n" <<
				"The mage is an esoteric class who starts weak, but gains considerable power over time.\n" <<
				"He uses his wand for basic attacks, and spells to further damage the enemy and support himself." << endl;
			break;
		}

		// Confirm class
		cout << "Choose this class?" << endl;

		if(InputUtil::promptYN())
			break;

		cout << "\nPlease choose another class:" << endl;
	}

	GameData* data = new GameData{name, Player(classType), areas[0]};

	saveFile(data);
	return data;
}

// Get list of saves and load
GameData* Save::loadSave(){
	return newSave();
}