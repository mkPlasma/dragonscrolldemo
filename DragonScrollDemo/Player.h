#pragma once

#include<iostream>
#include<vector>

#include "Entity.h"
#include "Item.h"


using std::vector;


static vector<string> playerClassNames = {"knight", "archer", "mage"};

struct InvItem{
	Item* item_;
	int count_;
};

class Player : public Entity{

	int classType_;

	int xp_, xpReq_, level_;

	int gold_;

	ItemWeapon* mainWeapon_;
	ItemWeapon* specialWeapon_;
	ItemEquippable* armor_;
	vector<InvItem> inventory_;
	int invSize_;

public:
	static enum types{KNIGHT, ARCHER, MAGE};

	Player() : Entity(){}
	Player(int type);

	void init();
	void updateStats();

	int getClassType();

	int getXp();
	int getXpReq();
	int getLevel();

	ItemWeapon* getMainWeapon();
	ItemWeapon* getSpecialWeapon();
	ItemEquippable* getArmor();
	vector<InvItem>* getInventory();
	int getInvSize();
};