#include "TextUtil.h"


// Set all characters to lowercase
string TextUtil::toLower(string& s){
	for(int i = 0; i < s.length(); ++i)
		s[i] = tolower(s[i]);
	return s;
}

// Trim surrounding whitespace and extra spaces
string TextUtil::trim(string& s){

	// Check for spaces at beginning/end of string
	if(regex_match(s, regex("^\\s.+$")))
		s.erase(0, s.find_first_not_of(' '));

	if(regex_match(s, regex("^.+\\s$")))
		s.erase(s.find_last_not_of(' ') + 1);

	// Remove extra whitespace
	s = regex_replace(s, regex("\\s+"), " ");

	return s;
}

// Standard format
string TextUtil::format(string& s){
	trim(s);
	toLower(s);
	return s;
}

// Capitalize first letter
string TextUtil::capitalize(string& s){
	s[0] = toupper(s[0]);
	return s;
}