#pragma once

#include<random>

#include "Save.h"
#include "Random.h"


class CommandHandler{

	vector<string> commands = {

		// 0
		"help",

		// 1
		"inventory",
		"items",
		"i",

		// 4
		"area",
		"a",
		"location",
		"l",
		"where",
		"w",

		// 10
		"use",
		"u",
		"eat",
		"drink",
		"consume",
		//15
		"equipment",
		"e",

		//17
		"drop",
		"d",
		// 19
		"stats",
		"s",

		// 21
		"health",
		"hp",

		// 23
		"magic",
		"mp",

		// 25
		"defense",
		"def",

		// 27
		"gold",
		"gp",
		"money",

		// 30
		"xp",
		"x",
		"experience",
		"level",

		// 32
		"shop",
		"buy",

		// 36
		"look",
		"l",

		// 38
		"interact",
		"in",

		// 40
		"goto",
		"g",

		// 42
		"hunt",
		"h",

		// 44
		"rest",
		"r",

		// 46
		"save",
		"sv",

		// 48
		"exit",
		"quit",


		// Debug commands

		// 50
		"give"
	};

	GameData* data_;
	Player* player_;
	Area* area_;

	Random rand;

	void stats();

	void inventory();

	void equip(string arg);

	void give(string arg);

	void randBattle();
	void battle(Enemy enemy);

public:
	CommandHandler(GameData* data);
	~CommandHandler();

	void mainLoop();

	void command();
};