#include "CommandHandler.h"

int main(){
	
	std::cout << "Hello and welcome to the Dragon Scroll demo!\nType 'start' to begin." << std::endl;

	string input = InputUtil::prompt();

	GameData* data = nullptr;

	if(input == "start")
		data = Save::loadSave();
	else{
		system("pause");
		return 0;
	}

	CommandHandler ch = *new CommandHandler(data);
	ch.mainLoop();

	delete &ch;
	
	system("pause");
	return 0;
}
